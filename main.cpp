#include <iostream>
#include <thread>
#include <stdio.h>
#include <stdio_ext.h>
#include "conio.h"
#include <mutex>



using namespace std;

int current_speed;
//int reached_speed;
mutex mut;



void Fonction_Regulateur(int reached_speed)
{
    mut.lock();

    bool arret_regulateur = false;

    cout << "Régulateur de Vitesse ON" << endl;


    while(!arret_regulateur)
    {
        //Modification de la vitesse due à une pente
        float facteur[19] = {0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09};

        srand(time(NULL));
        int id = rand()% 18;
        current_speed *= facteur[id];

        if(current_speed > reached_speed)
        {
            cout << "La route est descendante" << endl;
        }
        else if (current_speed < reached_speed)
        {
            cout << "La route est montante" << endl;
        }
        else
        {
            cout << "La route est plate" << endl;
        }

        //Régulation de la vitesse suite à la baisse ou l'augmentation de la vitesse
        while(current_speed!= reached_speed)
        {
            cout << "Votre vitesse: " << current_speed << "\tVitesse cible: " << reached_speed << endl;
            if(current_speed < reached_speed)
            {
                current_speed++;
            }
            else if (current_speed > reached_speed)
            {
                current_speed--;
            }
        }

        cout << "Votre vitesse: " << current_speed << "\tVitesse cible: " << reached_speed << endl;
        cout << "Vitesse de croisière atteinte" << endl;
        cout << "_______________________________________________" << endl;
        sleep(1);

        __fpurge(stdin);
        while(kbhit())
        {
            int d=getchar();
            //cout << d;
            if(d==112)                  // touche p pour une augmentation de la vitesse du régulateur
            {
                reached_speed++;
            }
            else if(d==109)             //touche m pour une diminution de la vitesse du régulateur
            {
                if(reached_speed > 40)
                {
                    reached_speed--;
                }
                else
                {
                    cout << "Attention! Vous ne pouvez baisser la vitesse de régulation sous 4Okm/h." << endl;
                }
            }
            else if(d==97 || d==102)    //touche a ou f pour une accélération ou freinage
            {
                arret_regulateur = true;
            }
        }

    }
    mut.unlock();

}


int main()
{
    int reached_speed = 0;
    current_speed = 0;
    int choix_conducteur;

    bool arret_voiture =  false;

    cout << "\t\t _______________________________________________" << endl;
    cout << "\t\t|\t\t\t\t\t\t|" << endl;
    cout << "\t\t|\tSimulateur de régulateur de vitesse\t|"<< endl;
    cout << "\t\t|_______________________________________________|\n\n" << endl;

    while(!arret_voiture)
    {
        if(current_speed == 0)
        {
            cout << "Veuillez sélectionner une vitesse" << endl;
            cin >> current_speed;
        }
        else
        {
            cout << "Régulateur de vitesse OFF" << endl;
            cout << "Vous roulez à la vitesse de " << current_speed << "km/h" << endl;
        }

        cout << "Choix conducteur" << endl;
        cout << "1-Enclencher le régulateur de vitesse" << endl;
        cout << "0-Arret voiture"<< endl;
        cin >> choix_conducteur;

        switch(choix_conducteur)
        {
            case 1:
            {
                if(current_speed < 40)
                {
                    cout << "Votre vitesse est trop faible pour enclencher le régulateur" << endl;
                    cout << "Veuillez augmenter votre vitesse" << endl;
                    do
                    {
                        cin >> current_speed;
                    }while(current_speed < 40);
                }

                //Initialisation de la vitesse du régulateur
                reached_speed = current_speed;

                thread t2(Fonction_Regulateur,reached_speed);
                t2.join();
            }
            break;
            case 0:
            {
                arret_voiture = true;

            }
        }
    }

    return 0;
}
