#include "control_vitesse.h"

control_vitesse::control_vitesse(int ini_velo)
{
    this->initial_velocity=ini_velo;
    this->velocity=ini_velo;
    this->min_velocity=40;
    this->max_velocity=130;
    this->initial_slope=0;
    this->slope=0;
}


void control_vitesse::change_slope()
{

}

void control_vitesse::change_slope_up()
{

}

void control_vitesse::change_slope_down()
{

}

void control_vitesse::set_ini_velocity(int ini_ve)
{
    this->initial_velocity=ini_ve;
}

void control_vitesse::set_velocity(int vel)
{
    this->velocity=vel;
}

void control_vitesse::set_slope(int slop)
{
    this->slope=slop;
}

int control_vitesse::get_ini_velocity()
{
   return initial_velocity;
}

int control_vitesse::get_velocity()
{
   return velocity;
}
int control_vitesse::get_min_velocity()
{
   return min_velocity;
}

int control_vitesse::get_max_velocity()
{
   return max_velocity;
}
int control_vitesse::get_slope()
{
   return slope;
}
