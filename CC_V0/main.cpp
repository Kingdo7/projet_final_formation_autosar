#include <iostream>
#include "control_vitesse.h"

using namespace std;

int main()
{
    //Variables declaration
    bool control_running=1;//Set to true when the car is running


    cout<<endl<<"********Welcome to the Cruise Control System********\n"<<endl;
    cout<<"Please, note that, for safety reasons, the minimum velocity you can set is 40 Km/h and the maximum velocity is 130 Km/h"<<endl;
    cout <<endl<< "Please, set the cruise velocity for your car in Km/h(number from 40 to 130) :" << endl;
    int ini_velo=0;
    cin>>ini_velo;

    control_vitesse control(ini_velo);

    while (ini_velo<40||ini_velo>130){
        cout<<endl<<"The velocity is too slow or too high, please introduce a velocity value between 40 to 130 :"<<endl;
        cin>>ini_velo;
    }

    while(control_running){

        //Block controlling if the motor needs to accelerate or slow down due a change of velocity in the cruise control system
        cout<<endl<<"Do you want to modify the Cruise Control Velocity (y/n)?"<<endl;
        char run_3[2];
        cin>>run_3;
        if (run_3[0]=='y'){
            cout<<endl<<"Please, introduce '+' to increase the velocity 5km/h or '-' to decrease it"<<endl;
            char run_4[2];
            cin>>run_4;
            if (run_4[0]=='+'){
                if ((control.get_velocity()+5)<=control.get_max_velocity()){
                    control.set_velocity(control.get_velocity()+5);
                    cout<<endl<<"The car is accelerating. The cruise control is set to "<<control.get_velocity()<<endl;
                }
                else{
                    cout<< endl<<"The cruise control velocity is already set to the maximum velocity. You can not increase the velocity."<<endl;
                }
            }
            else if (run_4[0]=='-'){
                 if ((control.get_velocity()-5)>=control.get_min_velocity()){
                    control.set_velocity(control.get_velocity()-5);
                    cout<<endl<<"The car is slowing down. The cruise control is set to "<<control.get_velocity()<<endl;
                 }
                 else{
                    cout<< endl<<"The cruise control velocity is already set to the minimum velocity. You can not decrease the velocity."<<endl;
                 }

            }
            else{
                cout<<endl<<"The car keeps its velocity to "<<control.get_velocity();
            }
        }
        //End of block that change velocity on the cruise control system


        //Block controlling changes in the slope,
        //therefore controlling if the car needs to accelerate or slow down
        //to meet the requirements of the velocity set in cruise control sytem

        cout<<endl<<"Is there a change in the slope of the road(y/n)?"<<endl;
        char run_5[2];
        cin>>run_5;
        if (run_5[0]=='y'){
            cout<<endl<<"The change in the slope is positive or negative(-/+)?"<<endl;
            char run_6[2];
            cin>>run_6;
            cout<<endl<<"Please, introduce the change in the slope:(1-5)"<<endl;
            int var_slope=0;
            cin>>var_slope;
            //Setting the total slope of the road
            if (run_6[0]=='+'){
                control.set_slope(control.get_slope()+var_slope);
                cout<<endl<<"The slope has increase, the motor is accelerating to meet the cruise control velocity"<<endl;
            }
            else if (run_6[0]=='-'){
                 control.set_slope(control.get_slope()-var_slope);
                 cout<<endl<<"The slope has decrease, the motor is decelerating to meet the cruise control velocity"<<endl;
            }
            //Checking of the motor needs to accelerate or slow down to meet the cruise control velocity according the current slope
        }













        //Block controlling the deactivation of the cruise control system by touching the break pedal or the accelarator
        cout<<endl<<"Do you need  to break or to accelarate(y/n)?"<<endl;
        char run_1[2];
        cin>>run_1;
        if (run_1[0]=='y'){
           control_running=0;
            break;
        }

        //Block controlling the deactivation of the cruise control system by simply deactivating the option
        cout<<endl<<"Do you want to deactivated  the Cruise Control System (y/n)?"<<endl;
        char run_2[2];
        cin>>run_2;
        if (run_2[0]=='y') {
        control_running=0;
        break;
        }




        cout<<endl;

    }

    if (!control_running) cout<<endl<<"You have desactivated the Cruise Control System"<<endl;

    return 0;
}
