#ifndef CONTROL_VITESSE_H_INCLUDED
#define CONTROL_VITESSE_H_INCLUDED

class control_vitesse{

public:

control_vitesse(int);
void change_slope();
void change_slope_up();
void change_slope_down();
void set_ini_velocity(int);
void set_velocity(int);
void set_slope(int);
int get_ini_velocity();
int get_velocity();
int get_max_velocity();
int get_min_velocity();
int get_slope();


private:

int initial_velocity=40;//Minimun velocity for cruise control
int velocity;
int min_velocity;
int max_velocity;
float initial_slope;
float slope;

float init_coef_friction;

};



#endif // CONTROL_VITESSE_H_INCLUDED
