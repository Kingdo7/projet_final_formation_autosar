clear all; close all; clc
%% Script_Anas_Car.m

%--------------------------------------------------------------------------
% General Parameters
g0=9.81 %m/s^2
rho=1.225 % kg/m^3
% Car Parameters
CarMass=1500 % kg

%MaxForce=8000; %w

MaxForce=3000

Cd=0.24
Af=1.844*1.646 %m^2
%--------------------------------------------------------------------------
%% PD Controller Simple Version


V_target=80*1000/3600; % Km/h

Time_step=1;
Time_simulation=30*60

time_vector=[0:Time_step:Time_simulation]
MaxKm=10;
x_alpha=[0 3*rand(MaxKm,1)'];
% x_alpha=ones(1,MaxKm+1);
Kmetrage=[0:1:MaxKm];

figure
plot(Kmetrage,x_alpha)


v=zeros(1, length(time_vector));
Force=zeros(1, length(time_vector));
n_steps=length(time_vector);
x=zeros(1, length(time_vector));

Kp=1
Kd=0.1
KI=0.001

figure
hold on
for i=3:n_steps
    acc_car=Force(i-1)/CarMass;
    acc_drag=0.5*rho*Cd*Af/CarMass*(v(i-1))^2;
    Slope_road=x_alpha(find((Kmetrage-x(i-1)/1000)>0));
    acc_weight=g0*sind(Slope_road(1));
    e=V_target-v(i-1);
    de_dt=-(v(i-1)-v(i-2));
%    de_dt=-(acc_car-acc_drag-acc_weight);

    u_t=Kp*e+Kd*de_dt-KI*sum(v(1:i)-V_target);

    Force(i)=u_t*CarMass;
    if abs(Force(i))>MaxForce
        Force(i)=sign(Force(i))*MaxForce;
        u_t=Force(i)/CarMass;
    end


    v(i)=v(i-1)+(u_t-acc_drag-acc_weight)*Time_step;
    x(i)=x(i-1)+v(i-1)*Time_step;

    %plot(time_vector(1:i),v(1:i)*3600/1000)
    plot(x(1:i),v(1:i)*3600/1000)

end




figure
plot(time_vector,x/1000)

